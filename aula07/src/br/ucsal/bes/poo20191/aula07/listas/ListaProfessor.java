package br.ucsal.bes.poo20191.aula07.listas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListaProfessor {

	public static void main(String[] args) {

		List<Professor> professores = new ArrayList<>();

		Professor professor1 = new Professor();
		professor1.setNome("Claudio");
		professor1.setEmail("claudio@ucsal.br");
		professor1.setMatricula(123);
		professor1.setDataNascimento(new Date());

		professores.add(professor1);
		professores.add(new Professor("Antonio", 123, "antonio@ucsal.br", new Date(), new Date()));

		System.out.println(professores.get(0).getNome());
		System.out.println(professores.get(1).getNome());

	}

}
