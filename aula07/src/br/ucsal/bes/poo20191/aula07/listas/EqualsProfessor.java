package br.ucsal.bes.poo20191.aula07.listas;

public class EqualsProfessor {

	public static void main(String[] args) {

		Professor professor1 = new Professor("antonio", 123);
		Professor professor2 = new Professor("antonio", 123);
		Professor professor3 = professor1;

		// == comparar se s�o a mesma inst�ncia ou n�o
		if (professor1 == professor2) {
			System.out.println("professor1 aponta para a mesma inst�ncia que professor2 aponta");
		} else {
			System.out.println("professor1 N�O aponta para a mesma inst�ncia que professor2 aponta");
		}

		if (professor1 == professor3) {
			System.out.println("professor1 aponta para a mesma inst�ncia que professor3 aponta");
		} else {
			System.out.println("professor1 N�O aponta para a mesma inst�ncia que professor3 aponta");
		}
		
		if(professor1.equals(professor2)){
			System.out.println("professor1 � igual ao professor2");
		}else{
			System.out.println("professor1 � diferente ao professor2");
		}

	}

}
