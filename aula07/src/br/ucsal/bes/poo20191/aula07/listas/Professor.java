package br.ucsal.bes.poo20191.aula07.listas;

import java.util.Date;

public class Professor {

	private String nome;

	private Integer matricula;

	private String email;

	private Date dataNascimento;

	private Date dataContratacao;

	public Professor() {
	}

	public Professor(String nome, Integer matricula, String email, Date dataNascimento, Date dataContratacao) {
		this.nome = nome;
		this.matricula = matricula;
		this.email = email;
		this.dataNascimento = dataNascimento;
		this.dataContratacao = dataContratacao;
	}

	public Professor(String nome, Integer matricula) {
		this.nome = nome;
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Date getDataContratacao() {
		return dataContratacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Professor other = (Professor) obj;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

//	@Override
//	public boolean equals(Object obj) {
//		Professor prof = (Professor) obj;
//		if (prof.nome.equals(nome) && prof.matricula.equals(matricula)) {
//			return true;
//		}
//		return false;
//	}

	
	
}
