package br.ucsal.bes2019.poo.aula13.exemplo1;

public abstract class Imovel {

	private static Integer contador = 0;

	private Integer codigo; // gerar automaticamente

	private String endereco;

	private String bairro;

	private Double valor;

	private SituacaoImovelEnum situacao;

	public Imovel(String endereco, String bairro, Double valor, SituacaoImovelEnum situacao) {
		super();
		this.endereco = endereco;
		this.bairro = bairro;
		this.valor = valor;
		this.situacao = situacao;
		definirCodigo();
	}

	private void definirCodigo() {
		contador++;
		this.codigo = contador;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public SituacaoImovelEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoImovelEnum situacao) {
		this.situacao = situacao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void apresentar() {
		System.out.println("codigo=" + codigo);
		System.out.println("endereco=" + endereco);
		System.out.println("bairro=" + bairro);
		System.out.println("valor=" + valor);
		System.out.println("situacao=" + situacao);
	}

	public abstract Double calcularImposto();

}
