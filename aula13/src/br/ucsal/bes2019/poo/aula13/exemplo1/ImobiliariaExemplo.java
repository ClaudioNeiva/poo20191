package br.ucsal.bes2019.poo.aula13.exemplo1;

import java.util.ArrayList;
import java.util.List;

public class ImobiliariaExemplo {

	public static void main(String[] args) {

		Imovel imovel1 = new Apartamento("Rua E", "Bairro 10", 70000d, SituacaoImovelEnum.DISPONIVEL, 185d, 140d);
		//imovel1.apresentar();
		System.out.println(imovel1.toString());
		
//		List<Imovel> imoveis = new ArrayList<>();
//
//		imoveis.add(new Casa("Rua A", "Bairro 1", 10000d, SituacaoImovelEnum.DISPONIVEL, 200d));
//		imoveis.add(new Apartamento("Rua E", "Bairro 10", 70000d, SituacaoImovelEnum.DISPONIVEL, 185d, 140d));
//		imoveis.add(new Apartamento("Rua F", "Bairro 20", 45000d, SituacaoImovelEnum.DISPONIVEL, 85d, 40d));
//		imoveis.add(new Casa("Rua B", "Bairro 2", 40000d, SituacaoImovelEnum.DISPONIVEL, 150d));
//		imoveis.add(new Casa("Rua C", "Bairro 3", 50000d, SituacaoImovelEnum.DISPONIVEL, 320d));
//		imoveis.add(new Apartamento("Rua G", "Bairro 30", 18000d, SituacaoImovelEnum.DISPONIVEL, 25d, 14d));
//
//		listar(imoveis);
//		Double totalImpostos = totalizarImpostos(imoveis);
//		System.out.println("totalImpostos = " + totalImpostos);
	}

	private static Double totalizarImpostos(List<Imovel> imoveis) {
		Double total = 0d;
		for (Imovel imovel : imoveis) {
			total += imovel.calcularImposto();
		}
		return total;
	}

	private static void listar(List<Imovel> imoveis) {
		for (Imovel imovel : imoveis) {
			imovel.apresentar();
			System.out.println("#####################################");
		}
	}

}
