package br.ucsal.bes2019.poo.aula13.exemplo1;

public class Apartamento extends Imovel {

	private Double fracaoIdeal;

	private Double areaPrivativa;

	public Apartamento(String endereco, String bairro, Double valor, SituacaoImovelEnum situacao, Double fracaoIdeal,
			Double areaPrivativa) {
		super(endereco, bairro, valor, situacao);
		this.fracaoIdeal = fracaoIdeal;
		this.areaPrivativa = areaPrivativa;
	}

	public Double getFracaoIdeal() {
		return fracaoIdeal;
	}

	public void setFracaoIdeal(Double fracaoIdeal) {
		this.fracaoIdeal = fracaoIdeal;
	}

	public Double getAreaPrivativa() {
		return areaPrivativa;
	}

	public void setAreaPrivativa(Double areaPrivativa) {
		this.areaPrivativa = areaPrivativa;
	}

	@Override
	public void apresentar() {
		// Necess�rio se voc� quiser fazer a chamada ao apresentar() da
		// superclasse. Caso contr�rio, essa chamada ao apresentar da
		// superclasse N�O ocorrer�!
		super.apresentar();

		System.out.println("fracaoIdeal=" + fracaoIdeal);
		System.out.println("areaPrivativa=" + areaPrivativa);
	}

	
	@Override
	public String toString() {
		return "Apartamento [fracaoIdeal=" + fracaoIdeal + ", areaPrivativa=" + areaPrivativa + "]";
	}

	@Override
	public Double calcularImposto() {
		return fracaoIdeal * 15d + areaPrivativa * 40d;
	}
}
