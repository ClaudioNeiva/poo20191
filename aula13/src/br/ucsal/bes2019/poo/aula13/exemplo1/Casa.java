package br.ucsal.bes2019.poo.aula13.exemplo1;

public class Casa extends Imovel {

	private Double areaTerreno;

	public Casa(String endereco, String bairro, Double valor, SituacaoImovelEnum situacao, Double areaTerreno) {
		super(endereco, bairro, valor, situacao);
		this.areaTerreno = areaTerreno;
	}

	public Double getAreaTerreno() {
		return areaTerreno;
	}

	public void setAreaTerreno(Double areaTerreno) {
		this.areaTerreno = areaTerreno;
	}

	@Override
	public void apresentar() {
		super.apresentar();
		System.out.println("areaTerreno=" + areaTerreno);
	}

	@Override
	public Double calcularImposto() {
		return areaTerreno * 50d;
	}
	
}
