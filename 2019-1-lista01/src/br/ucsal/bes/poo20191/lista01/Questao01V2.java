package br.ucsal.bes.poo20191.lista01;

import java.util.Scanner;

//1. Crie um programa em Java que:
//a) Leia o nome;
//b) Leia o sobrenome;
//c) Concatene o nome com o sobrenome;
//d) Apresente o nome completo.
public class Questao01V2 {

	public static void main(String[] args) {
		// Declara��o de vari�veis
		String nome;
		String sobrenome;
		String nomeCompleto;

		// Entrada de dados
		nome = obterString("Informe o nome:");
		sobrenome = obterString("Informe o sobrenome:");
		
		// Processamento
		nomeCompleto = calcularNomeCompleto(nome, sobrenome);

		// Sa�da
		exibirNomeCompleto(nomeCompleto);
	}

	private static String obterString(String mensagem) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println(mensagem);
		return sc.nextLine();
	}

	private static void exibirNomeCompleto(String nomeCompleto) {
		System.out.println("Nome completo = " + nomeCompleto);
	}

	private static String calcularNomeCompleto(String nome, String sobrenome) {
		String nomeCompleto;
		nomeCompleto = calcularNome(nome, sobrenome);
		return nomeCompleto;
	}

	private static String calcularNome(String nome, String sobrenome) {
		String nomeCompleto;
		nomeCompleto = nome + " " + sobrenome;
		return nomeCompleto;
	}

}
