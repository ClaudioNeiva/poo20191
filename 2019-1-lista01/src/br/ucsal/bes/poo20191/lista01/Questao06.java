package br.ucsal.bes.poo20191.lista01;

import java.util.Scanner;

//Crie um programa em Java que permita a entrada de 10 valores em cada um dos dois
//vetores (vet1 e vet2). A seguir o programa dever� armazenar num terceiro vetor (vet3) a soma
//do conte�do dos dois vetores (vet1 e vet2). Por fim, o programa dever� exibir os valores
//armazenados em vet3.

public class Questao06 {

	private static final int QTD_NUMEROS = 10;

	public static void main(String[] args) {
		// Declara��o de vari�veis
		Integer vet1[] = new Integer[QTD_NUMEROS];
		Integer vet2[] = new Integer[QTD_NUMEROS];
		Integer vet3[] = new Integer[QTD_NUMEROS];

		// Entrada de dados
		obterNumeros("vetor 1", vet1);
		obterNumeros("vetor 2", vet2);

		// Processamento
		somarVetores(vet3, vet1, vet2);

		// Sa�da
		exibirVetor("A soma dos vetores 1 e 2 �:", vet3);
	}

	private static void obterNumeros(String nomeVetor, Integer[] vet) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Informe " + vet.length + " n�meros para " + nomeVetor + ":");
		for (Integer i = 0; i < vet.length; i++) {
			vet[i] = sc.nextInt();
		}
	}

	private static void somarVetores(Integer[] vetResultado, Integer[] vetA, Integer[] vetB) {
		for (Integer i = 0; i < vetResultado.length; i++) {
			vetResultado[i] = vetA[i] + vetB[i];
		}
	}

	private static void exibirVetor(String mensagem, Integer[] vet) {
		System.out.println(mensagem);
		for (Integer i = 0; i < vet.length; i++) {
			System.out.println(vet[i]);
		}
	}

}
