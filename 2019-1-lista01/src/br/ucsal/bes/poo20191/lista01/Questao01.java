package br.ucsal.bes.poo20191.lista01;

import java.util.Scanner;

import javax.sound.midi.SysexMessage;

//1. Crie um programa em Java que:
//a) Leia o nome;
//b) Leia o sobrenome;
//c) Concatene o nome com o sobrenome;
//d) Apresente o nome completo.
public class Questao01 {

	public static void main(String[] args) {
		// Declara��o de vari�veis
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String nome;
		String sobrenome;
		String nomeCompleto;

		// Entrada de dados
		System.out.println("Informe o nome:");
		nome = sc.nextLine();
		System.out.println("Informe o sobrenome:");
		sobrenome = sc.nextLine();

		// Processamento
		nomeCompleto = nome + " " + sobrenome;

		// Sa�da
		System.out.println("Nome completo = " + nomeCompleto);
	}

}
