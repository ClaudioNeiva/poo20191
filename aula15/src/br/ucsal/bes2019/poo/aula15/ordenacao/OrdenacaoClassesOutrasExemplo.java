package br.ucsal.bes2019.poo.aula15.ordenacao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OrdenacaoClassesOutrasExemplo {

	public static void main(String[] args) {
		List<Pessoa> pessoas = criarListaPessoasAdicionandoItens();
		ordenarAteJava7(pessoas);
		ordenarJava8(pessoas);
	}

	private static void ordenarAteJava7(List<Pessoa> pessoas) {
		System.out.println("Pessoas como foram informados:\n" + pessoas);

		Collections.sort(pessoas);
		System.out.println("*********************************************************");
		System.out.println("Pessoas em ordem crescente de ano nascimento:\n" + pessoas);

		// Collections.reverse inverte a ordem dos elementos da lista, como ela
		// estava em ordem crescente, reverter significa deix�-la em ordem
		// decrescente.
		Collections.reverse(pessoas);
		System.out.println("*********************************************************");
		System.out.println("Nomes em ordem decrescente de ano nascimento:\n" + pessoas);

		// Pode, mas n�o � muito visto.
		Comparator<Pessoa> comparadorNome = new ComparadorNomes();
		Collections.sort(pessoas, comparadorNome);
		System.out.println("*********************************************************");
		System.out.println("Pessoas em ordem crescente de nome:\n" + pessoas);

		// Essa � a forma que � vista.
		Comparator<Pessoa> comparadorNomeInLine = new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa o1, Pessoa o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		};
		Collections.sort(pessoas, comparadorNomeInLine);
		System.out.println("*********************************************************");
		System.out.println("Pessoas em ordem crescente de nome:\n" + pessoas);

		// Essa � a forma que � REALMENTE vista.
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa o1, Pessoa o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});
		System.out.println("*********************************************************");
		System.out.println("Pessoas em ordem crescente de nome:\n" + pessoas);

		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa o1, Pessoa o2) {
				int comparacao = o1.getSexo().compareTo(o2.getSexo());
				if (comparacao == 0) {
					comparacao = o1.getNome().compareTo(o2.getNome());
				}
				return comparacao;
			}
		});
		System.out.println("*********************************************************");
		System.out.println("Pessoas em ordem crescente de sexo, depois por nome:\n" + pessoas);

	}

	private static void ordenarJava8(List<Pessoa> pessoas) {
		pessoas.sort(new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa o1, Pessoa o2) {
				int comparacao = o1.getSexo().compareTo(o2.getSexo());
				if (comparacao == 0) {
					comparacao = o1.getAnoNascimento().compareTo(o2.getAnoNascimento());
				}
				return comparacao;
			}
		});
		System.out.println("*********************************************************JAVA8");
		System.out.println("Pessoas em ordem crescente de sexo, depois por ano nascimento:\n" + pessoas);

		pessoas.sort((o1, o2) -> {
			return o1.getAnoNascimento().compareTo(o2.getAnoNascimento());
		});
		System.out.println("*********************************************************JAVA8");
		System.out.println("Pessoas em ordem crescente de ano nascimento:\n" + pessoas);
	}

	public static List<Pessoa> criarListaPessoasComArrays() {
		List<Pessoa> pessoas = Arrays.asList(new Pessoa("Antonio", 1990, SexoEnum.MASCULINO),
				new Pessoa("Maria", 1995, SexoEnum.FEMININO), new Pessoa("Ana", 1993, SexoEnum.FEMININO),
				new Pessoa("Pedro", 1997, SexoEnum.MASCULINO), new Pessoa("Carla", 1995, SexoEnum.FEMININO));
		return pessoas;
	}

	public static List<Pessoa> criarListaPessoasAdicionandoItens() {
		List<Pessoa> pessoas = new ArrayList<>();
		pessoas.add(new Pessoa("Antonio", 1990, SexoEnum.MASCULINO));
		pessoas.add(new Pessoa("Maria", 1995, SexoEnum.FEMININO));
		pessoas.add(new Pessoa("Ana", 1993, SexoEnum.FEMININO));
		pessoas.add(new Pessoa("Pedro", 1997, SexoEnum.MASCULINO));
		pessoas.add(new Pessoa("Carla", 1995, SexoEnum.FEMININO));
		return pessoas;
	}

}
