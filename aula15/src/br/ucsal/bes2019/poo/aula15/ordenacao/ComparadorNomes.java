package br.ucsal.bes2019.poo.aula15.ordenacao;

import java.util.Comparator;

// Pode, mas n�o � muito visto.
public class ComparadorNomes implements Comparator<Pessoa> {

	@Override
	public int compare(Pessoa o1, Pessoa o2) {
		return o1.getNome().compareTo(o2.getNome());
	}

}
