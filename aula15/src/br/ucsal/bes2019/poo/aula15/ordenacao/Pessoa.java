package br.ucsal.bes2019.poo.aula15.ordenacao;

public class Pessoa implements Comparable<Pessoa> {

	private String nome;

	private Integer anoNascimento;

	private SexoEnum sexo;

	public Pessoa(String nome, Integer anoNascimento, SexoEnum sexo) {
		super();
		this.nome = nome;
		this.anoNascimento = anoNascimento;
		this.sexo = sexo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public SexoEnum getSexo() {
		return sexo;
	}

	public void setSexo(SexoEnum sexo) {
		this.sexo = sexo;
	}

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", anoNascimento=" + anoNascimento + ", sexo=" + sexo + "]\n";
	}

	@Override
	// Ordernar pelo ano de nascimento
	// Ordena��o default!
	public int compareTo(Pessoa o) {
		return anoNascimento.compareTo(o.anoNascimento);
		// int resultado = anoNascimento.compareTo(o.anoNascimento);
		//
		// int resultado1 = anoNascimento > o.anoNascimento ? 1 : anoNascimento
		// < o.anoNascimento ? -1 : 0;
		//
		// if (anoNascimento > o.anoNascimento) {
		// return 1;
		// } else if (anoNascimento < o.anoNascimento) {
		// return -1;
		// } else {
		// return 0;
		// }
	}

}
