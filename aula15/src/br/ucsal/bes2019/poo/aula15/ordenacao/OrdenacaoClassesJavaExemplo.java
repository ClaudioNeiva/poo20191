package br.ucsal.bes2019.poo.aula15.ordenacao;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class OrdenacaoClassesJavaExemplo {

	public static void main(String[] args) {
		ordenarAteJava7();
	}

	private static void ordenarAteJava7() {
		List<String> nomes = Arrays.asList("Claudio", "Maria", "Antonio", "Pedro", "Joaquim", "Clara");
		System.out.println("Nomes como foram informados:" + nomes);

		Collections.sort(nomes);
		System.out.println("Nomes em ordem crescente:" + nomes);

		// Collections.reverse inverte a ordem dos elementos da lista, como ela
		// estava em ordem crescente, reverter significa deix�-la em ordem
		// decrescente.
		Collections.reverse(nomes);
		System.out.println("Nomes em ordem decrescente:" + nomes);
	}

}
