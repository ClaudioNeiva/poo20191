package br.ucsal.bes2019.poo.aula15.excecao;

public class CalculoBO {

	public static Long calcularFatorial(Integer n) throws Exception {
		if (n < 0 || n > 10) {
			throw new Exception("O valor de n n�o � v�lido. N deve estar entre 0 e 10, intervalo fechado.");
		}
		Long fat = 1L;
		for (Integer i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

}
