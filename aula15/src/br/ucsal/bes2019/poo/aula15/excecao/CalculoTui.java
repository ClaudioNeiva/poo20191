package br.ucsal.bes2019.poo.aula15.excecao;

import java.util.Scanner;

public class CalculoTui {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		Integer n = obterNumero("Informe um n�mero entre 0 e 10:");
		try {
			Long fatorial = calcular(n);
			exibirFatorial(n, fatorial);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Cheguei no fim!");
	}

	private static Long calcular(Integer n) throws Exception {
		Long fat = CalculoBO.calcularFatorial(n);
		return fat;
	}

	private static Integer obterNumero(String mensagem) {
		System.out.println(mensagem);
		return scanner.nextInt();
	}

	private static void exibirFatorial(Integer n, Long fatorial) {
		System.out.println("fatorial(" + n + ")=" + fatorial);
	}

}
