package br.ucsal.bes2019.poo.aula15.diversos;

import java.util.ArrayList;
import java.util.List;

public class QuantidadeParametrosVariadaExemplo {

	public static void main(String[] args) {
		List<String> nomes1 = criarListaString();
		List<String> nomes2 = criarListaString("claudio");
		List<String> nomes3 = criarListaString("claudio", "antonio", "ana", "pedro");
		System.out.println("nomes1=" + nomes1);
		System.out.println("nomes2=" + nomes2);
		System.out.println("nomes3=" + nomes3);
	}

	private static List<String> criarListaString(String... vetorStrings) {
		List<String> strings = new ArrayList<>();
		for (String string : vetorStrings) {
			strings.add(string);
		}
		return strings;
	}

}
