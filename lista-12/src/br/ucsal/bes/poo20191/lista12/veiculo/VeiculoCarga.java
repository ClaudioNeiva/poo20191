package br.ucsal.bes.poo20191.lista12.veiculo;

public class VeiculoCarga extends Veiculo {
	private Integer capacidadeMaximaCarga;
	private Integer qtdEixos;
	private Integer capacidadeTanque;

	public VeiculoCarga(Integer capacidadeMaximaCarga, Integer qtdEixos, Integer capacidadeTanque) {
		super();
		this.capacidadeMaximaCarga = capacidadeMaximaCarga;
		this.qtdEixos = qtdEixos;
		this.capacidadeTanque = capacidadeTanque;
	}

	public Integer getCapacidadeMaximaCarga() {
		return capacidadeMaximaCarga;
	}

	public void setCapacidadeMaximaCarga(Integer capacidadeMaximaCarga) {
		this.capacidadeMaximaCarga = capacidadeMaximaCarga;
	}

	public Integer getQtdEixos() {
		return qtdEixos;
	}

	public void setQtdEixos(Integer qtdEixos) {
		this.qtdEixos = qtdEixos;
	}

	public Integer getCapacidadeTanque() {
		return capacidadeTanque;
	}

	public void setCapacidadeTanque(Integer capacidadeTanque) {
		this.capacidadeTanque = capacidadeTanque;
	}

}
