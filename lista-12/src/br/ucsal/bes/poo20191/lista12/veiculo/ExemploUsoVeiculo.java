package br.ucsal.bes.poo20191.lista12.veiculo;

import java.util.ArrayList;
import java.util.List;

public class ExemploUsoVeiculo {

	public static void main(String[] args) {
		List<Veiculo> veiculos = new ArrayList<>();
		veiculos.add(new Moto(CategoriaMotoEnum.URBANA, 125));
		veiculos.add(new VeiculosPasseio(5, 520));
		veiculos.add(new VeiculoCarga(10, 3, 300));

		((Moto) veiculos.get(0)).setQtdCilindradas(150);

		Moto moto1 = new Moto(CategoriaMotoEnum.TRILHA, 250);

		if (moto1.getCategoria().equals(CategoriaMotoEnum.TRILHA)) {
			System.out.println("Vai sujar!");
		}
		if (CategoriaMotoEnum.TRILHA.equals(moto1.getCategoria())) {
			System.out.println("Vai sujar!");
		}

	}

	public void exibirDetalhes(Veiculo veiculo) {
		// Como saber qual o tipo de ve�culo???
		// Vamos aprender depois o conceito de polimorfismo e entender porque a
		// pergunta instanceof n�o � muito utilizada
		if (veiculo instanceof Moto) {
			System.out.println("Ele � uma moto!");
		}
		if (veiculo instanceof VeiculoCarga) {
			System.out.println("Ele � uma ve�culo de carga!");
		}
	}

}
