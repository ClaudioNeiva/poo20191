package br.ucsal.bes.poo20191.lista12.veiculo;

public class VeiculosPasseio extends Veiculo {
	private Integer qtdMaximaPassageiros;
	private Integer capacidadePortaMalas;

	public VeiculosPasseio(Integer qtdMaximaPassageiros, Integer capacidadePortaMalas) {
		super();
		this.qtdMaximaPassageiros = qtdMaximaPassageiros;
		this.capacidadePortaMalas = capacidadePortaMalas;
	}

	public Integer getQtdMaximaPassageiros() {
		return qtdMaximaPassageiros;
	}

	public void setQtdMaximaPassageiros(Integer qtdMaximaPassageiros) {
		this.qtdMaximaPassageiros = qtdMaximaPassageiros;
	}

	public Integer getCapacidadePortaMalas() {
		return capacidadePortaMalas;
	}

	public void setCapacidadePortaMalas(Integer capacidadePortaMalas) {
		this.capacidadePortaMalas = capacidadePortaMalas;
	}

}
