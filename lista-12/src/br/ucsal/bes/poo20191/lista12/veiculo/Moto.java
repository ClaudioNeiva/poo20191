package br.ucsal.bes.poo20191.lista12.veiculo;

public class Moto extends Veiculo {
	
	private CategoriaMotoEnum categoria;
	private Integer qtdCilindradas;

	public Moto(CategoriaMotoEnum categoria, Integer qtdCilindradas) {
		super();
		this.categoria = categoria;
		this.qtdCilindradas = qtdCilindradas;
	}

	public CategoriaMotoEnum getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaMotoEnum categoria) {
		this.categoria = categoria;
	}

	public Integer getQtdCilindradas() {
		return qtdCilindradas;
	}

	public void setQtdCilindradas(Integer qtdCilindradas) {
		this.qtdCilindradas = qtdCilindradas;
	}

}
