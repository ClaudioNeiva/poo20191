package br.ucsal.bes.poo20191.aula04.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes.poo20191.aula04.domain.Professor;

public class ProfessorDAO {

	private static List<Professor> professores = new ArrayList<>();

	public static void inserir(Professor professor) {
		professores.add(professor);
	}
	
	public static int consultarQtdProfessoresArmazenados(){
		return professores.size();
	}

	public static Professor consultar(Integer matricula) {
		for (Professor professor : professores) {
			if (professor.getMatricula().equals(matricula)) {
				return professor;
			}
		}
		return null;
	}

}
