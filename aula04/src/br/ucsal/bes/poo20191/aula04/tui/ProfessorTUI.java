package br.ucsal.bes.poo20191.aula04.tui;

import java.util.Scanner;

import br.ucsal.bes.poo20191.aula04.business.ProfessorBO;
import br.ucsal.bes.poo20191.aula04.domain.Professor;

public class ProfessorTUI {

	public static void main(String[] args) {
		Professor professor;
		String mensagem;

		professor = ProfessorTUI.obterDados();
		mensagem = ProfessorBO.cadastrar(professor);
		System.out.println("Mensagem=" + mensagem);

		professor = ProfessorTUI.obterDados();
		ProfessorBO.cadastrar(professor);
		System.out.println("Mensagem=" + mensagem);

		consultar();
	}

	private static void consultar() {
		Scanner sc = new Scanner(System.in);
		Integer matricula;
		Professor professor;
		while (true) {
			System.out.println("Informe a matr�cula:");
			matricula = sc.nextInt();
			professor = ProfessorBO.consultar(matricula);
			if (professor == null) {
				System.out.println("Professor n�o encontrado");
			} else {
				System.out.println("Nome=" + professor.getNome());
			}
		}
	}

	private static Professor obterDados() {
		Scanner sc = new Scanner(System.in);
		String nome;
		Integer matricula;
		String email;

		System.out.println("Informe o nome:");
		nome = sc.nextLine();
		System.out.println("Informe a matr�cula:");
		matricula = sc.nextInt();
		sc.nextLine();
		System.out.println("Informe o email:");
		email = sc.nextLine();
		System.out.println("nome=" + nome);
		System.out.println("matricula=" + matricula);
		System.out.println("email=" + email);

		Professor professor = new Professor();
		professor.setNome(nome);
		professor.setMatricula(matricula);
		professor.setEmail(email);

		return professor;
	}

}
