package br.ucsal.bes.poo20191.aula04.business;

import br.ucsal.bes.poo20191.aula04.domain.Professor;
import br.ucsal.bes.poo20191.aula04.persistence.ProfessorDAO;

public class ProfessorBO {

	public static String cadastrar(Professor professor) {
		
		if (professor == null) {
			// FIXME Mudar para utilizar exception!
			return "Erro: professor n�o informado.";
		}
		if (professor.getNome() != null && professor.getNome().trim().isEmpty()) {
			// FIXME Mudar para utilizar exception!
			return "Erro: nome do professor n�o informado.";
		}
		if (professor.getMatricula() == null) {
			// FIXME Mudar para utilizar exception!
			return "Erro: nome do professor n�o informado.";
		}
		
		ProfessorDAO.inserir(professor);
		
		return "Sucesso: professor cadastrado!";
	}

	public static Professor consultar(Integer matricula) {
		return ProfessorDAO.consultar(matricula);
	}

}
