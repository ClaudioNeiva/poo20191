package br.ucsal.bes.poo20191.aula03;

public class Escola {

	public static void main(String[] args) {

		Professor professor1 = new Professor();
		Professor professor2 = new Professor();

		professor1.nome = "claudio";
		professor1.matricula = 123;
		professor1.email = "antonio.neiva@pro.ucsal.br";

		professor2.nome = "osvaldo";
		professor2.matricula = 456;
		professor2.email = "osvaldo@pro.ucsal.br";

		System.out.println("professor1=" + professor1);
		System.out.println("professor2=" + professor2);

		System.out.println("professor1.nome=" + professor1.nome);
		System.out.println("professor2.nome=" + professor2.nome);
		
	}

}


