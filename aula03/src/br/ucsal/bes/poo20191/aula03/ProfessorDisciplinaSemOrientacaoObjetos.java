package br.ucsal.bes.poo20191.aula03;

import java.util.Date;

public class ProfessorDisciplinaSemOrientacaoObjetos {

	private static final int QTD_PROFESSOR = 10;
	private static final int QTD_DISCIPLINA = 5;

	public static void main(String[] args) {

		String nomesProfessores[] = new String[QTD_PROFESSOR];
		Integer matriculasProfessores[] = new Integer[QTD_PROFESSOR];
		String emailsProfessores[] = new String[QTD_PROFESSOR];
		Date datasNascimentoProfessores[] = new Date[QTD_PROFESSOR];
		Date datasContratacaoProfessor[] = new Date[QTD_PROFESSOR];

		String codigosDisciplinas[] = new String[QTD_DISCIPLINA];
		String nomesDisciplinas[] = new String[QTD_DISCIPLINA];
		Integer cargasHorariasDisciplinas[] = new Integer[QTD_DISCIPLINA];

		cadastrarProfessores(nomesProfessores, matriculasProfessores, emailsProfessores, datasNascimentoProfessores, datasContratacaoProfessor);

		cadastrarDisciplinas(codigosDisciplinas, nomesDisciplinas, cargasHorariasDisciplinas);

	}

	private static void cadastrarDisciplinas(String[] codigosDisciplinas, String[] nomesDisciplinas,
			Integer[] cargasHorariasDisciplinas) {
	}

	private static void cadastrarProfessores(String[] nomesProfessores, Integer[] matriculasProfessores,
			String[] emailsProfessores, Date[] datasNascimentoProfessores, Date[] datasContratacaoProfessor) {
	}

}
