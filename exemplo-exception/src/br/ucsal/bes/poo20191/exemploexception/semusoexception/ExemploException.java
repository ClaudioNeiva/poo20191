package br.ucsal.bes.poo20191.exemploexception.semusoexception;

import java.util.Scanner;

// Voc� N�O deve fazer assim!!!
public class ExemploException {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		Integer n;
		Long fatorial;

		n = obterNumero();
		RetornoFatorial retornoFatorial = calcularFatorial(n);
		if (retornoFatorial.getCodigoErro() == 0) {
			exibirFatorial(n, retornoFatorial.getFatorial());
		} else {
			System.out.println("Erro ao calcular fatorial (c�digo=" + retornoFatorial.getCodigoErro() + ").");
		}

	}

	private static Integer obterNumero() {
		System.out.println("Informe um n�mero:");
		return scanner.nextInt();
	}

	private static RetornoFatorial calcularFatorial(Integer n) {
		if (n < 0) {
			return new RetornoFatorial(-1, null);
		}
		Long fat = 1L;
		for (Integer i = 1; i <= n; i++) {
			fat *= i;
		}
		return new RetornoFatorial(0, fat);
	}

	private static void exibirFatorial(Integer n, Long fatorial) {
		System.out.println("Fatorial(" + n + ")=" + fatorial);
	}

}
