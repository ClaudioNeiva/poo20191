package br.ucsal.bes.poo20191.exemploexception.semusoexception;

//Voc� N�O deve fazer assim!!!
public class RetornoFatorial {

	private Integer codigoErro;

	private Long fatorial;

	public RetornoFatorial(Integer codigoErro, Long fatorial) {
		super();
		this.codigoErro = codigoErro;
		this.fatorial = fatorial;
	}

	public Integer getCodigoErro() {
		return codigoErro;
	}

	public void setCodigoErro(Integer codigoErro) {
		this.codigoErro = codigoErro;
	}

	public Long getFatorial() {
		return fatorial;
	}

	public void setFatorial(Long fatorial) {
		this.fatorial = fatorial;
	}

}
