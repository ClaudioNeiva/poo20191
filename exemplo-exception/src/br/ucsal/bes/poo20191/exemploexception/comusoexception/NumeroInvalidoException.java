package br.ucsal.bes.poo20191.exemploexception.comusoexception;

public class NumeroInvalidoException extends Exception{

	private static final long serialVersionUID = 1L;

	public NumeroInvalidoException(String message) {
		super(message);
	}
	
}
