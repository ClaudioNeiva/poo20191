package br.ucsal.bes.poo20191.exemploexception.comusoexception;

import java.util.Scanner;

public class ExemploException {

	private static Scanner scanner = new Scanner(System.in);

	// public static void main(String[] args) throws NumeroInvalidoException {

	public static void main(String[] args) {
		Integer n;
		Long fatorial;

		n = obterNumero();
		try {
			fatorial = calcularFatorial(n);
			exibirFatorial(n, fatorial);
		} catch (NumeroInvalidoException e) {
			System.err.println(e.getMessage());
		}
		System.out.println("Fim do programa!");
	}

	private static Integer obterNumero() {
		System.out.println("Informe um n�mero:");
		return scanner.nextInt();
	}

	private static Long calcularFatorial(Integer n) throws NumeroInvalidoException {
		if (n < 0) {
			throw new NumeroInvalidoException("N�o � poss�vel calcular fatorial de n�meros negativos.");
		}
		Long fat = 1L;
		for (Integer i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

	private static void exibirFatorial(Integer n, Long fatorial) {
		System.out.println("Fatorial(" + n + ")=" + fatorial);
	}

}
