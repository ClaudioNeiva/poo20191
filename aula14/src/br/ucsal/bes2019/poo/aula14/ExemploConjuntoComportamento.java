package br.ucsal.bes2019.poo.aula14;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ExemploConjuntoComportamento {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		Pessoa pessoa1 = new Pessoa("ana", "123", 2000);
		Pessoa pessoa2 = new Pessoa("maria", "546", 2001);
		Pessoa pessoa3 = new Pessoa("clara", "890", 1950);

		Set<Pessoa> pessoas = new HashSet<>();
		System.out.println("Adicionando elementos...");
		pessoas.add(pessoa1);
		pessoas.add(pessoa2);
		pessoas.add(pessoa3);
		System.out.println("Elementos adicionados.");
		
		pessoa1.setNome("antonio");

		System.out.println("Pessoas:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		System.out.println("Comparando as pessoas:");
		for (Pessoa pessoa : pessoas) {
			if (pessoa.equals(pessoa1)) {
				System.out.println("Achei 1 pessoa1");
			}
		}

		System.out.println("Chamando contains...");
		System.out.println("pessoas.contains(pessoa1)=" + pessoas.contains(pessoa1));
		System.out.println("Fim.");

	}

}
