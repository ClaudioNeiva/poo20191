package br.ucsal.bes2019.poo.aula14;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ExemploConjunto {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		Set<Pessoa> pessoas = new HashSet<>();
		Pessoa pessoaInformada;

		System.out.println("Informe 3 pessoas diferentes:");
		do {
			pessoaInformada = obterPessoa();
			pessoas.add(pessoaInformada);
		} while (pessoas.size() < 3);

		System.out.println("Os 3 pessoas diferentes foram:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

	}

	private static Pessoa obterPessoa() {
		String nome;
		String telefone;
		Integer anoNascimento;

		System.out.println("Informe o nome:");
		nome = scanner.nextLine();
		System.out.println("Informe o telefone:");
		telefone = scanner.nextLine();
		System.out.println("Informe o ano nascimento:");
		anoNascimento = scanner.nextInt();
		scanner.nextLine();

		return new Pessoa(nome, telefone, anoNascimento);
	}

}
