package br.ucsal.bes2019.poo.aula14;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ExemploMap {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		Integer n;

		// Map<key, value>
		// Map<valor, qtd-ocorrencias>
		Map<Integer, Integer> valores = new HashMap<>();

		System.out.println("Informe 10 n�meros:");
		for (int i = 0; i < 10; i++) {
			n = scanner.nextInt();
			if (!valores.containsKey(n)) {
				// valores.put(key, value)
				valores.put(n, 1);
			} else {
				// valores.get(key)
				Integer qtdAtual = valores.get(n);
				valores.put(n, qtdAtual + 1);
			}
		}

		System.out.println("Os distintos n�meros informados e suas respectivas quantidades s�o:");
		// valores.keySet() -> o conjunto de chaves
		for (Integer key : valores.keySet()) {
			System.out.println(key + "=" + valores.get(key));
		}
	}

	private void seiLa() {
		// Map<sigla-estado, lista-cidades-daquele-estado>
		Map<String, List<String>> cidadesPorEstado;

		// Map<sigla-pa�s, Map<sigla-estado, lista-cidades-daquele-estado>>
		Map<String, Map<String, List<String>>> cidadesPorEstadoPorPais;
	}

}
