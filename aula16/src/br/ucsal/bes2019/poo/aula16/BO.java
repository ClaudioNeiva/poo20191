package br.ucsal.bes2019.poo.aula16;

public class BO {

	public static Long calcularFatorial(Integer n) throws Exception{
		Long fatorial = CalculoUtil.calcularFatorial(n);
		return fatorial;
	}
	
}
