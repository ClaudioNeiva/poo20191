package br.ucsal.bes2019.poo.aula16;

import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

public class Sorteio {

	private final static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		Random random = new Random();
		Set<Integer> numerosSorteados = new HashSet<>();
		do {
			int n = random.nextInt(27) + 1;
			System.out.println("n=" + n);
			scanner.nextLine();
			numerosSorteados.add(n);
		} while (numerosSorteados.size() < 13);
	}

}
