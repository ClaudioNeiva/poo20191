package br.ucsal.bes2019.poo.aula16;

public class NegocioException extends Exception {

	private static final long serialVersionUID = 1L;

	public NegocioException(String message) {
		super(message);
	}

	
}
