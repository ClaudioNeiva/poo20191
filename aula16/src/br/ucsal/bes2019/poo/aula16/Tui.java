package br.ucsal.bes2019.poo.aula16;

import java.util.Scanner;

public class Tui {

	private final static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Informe um n�mero para c�lculo do fatorial:");
		Integer n = scanner.nextInt();
		try {
			Long fatorial = CalculoUtil.calcularFatorial(n);
			System.out.println("fatorial(" + n + ")=" + fatorial);
		} catch (NegocioException e) {
			System.out.println("Erro da minha aplica��o: " + e.getMessage());
		} catch (Exception e) {
			System.out.println("Erro n�o espec�fico: " + e.getMessage());
		} finally {
			System.out.println("Aqui sempre vai acontecer.");
		}
	}

}
