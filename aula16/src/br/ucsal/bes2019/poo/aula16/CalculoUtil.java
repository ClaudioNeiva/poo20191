package br.ucsal.bes2019.poo.aula16;

public class CalculoUtil {

	// Exception -> checked
	// RuntimeExcetpion -> unchecked
	
	public static Long calcularFatorial(Integer n) throws NegocioException {
		if (n < 0) {
			throw new NegocioException("N�o � poss�vel calcular o fatorial de n�meros negativos.");
		}
		Long fat = 1L;
		for (Integer i = 1; i <= n; i++) {
			fat *= i; // fat = fat * i;
		}
		return fat;
	}

}
