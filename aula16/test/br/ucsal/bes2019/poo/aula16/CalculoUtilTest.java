package br.ucsal.bes2019.poo.aula16;

import org.junit.Assert;
import org.junit.Test;

public class CalculoUtilTest {

	@Test
	public void testarFatorial5() throws NegocioException {
		// Definir dados de entrada
		Integer n = 5;
		// Definir a sa�da esperada
		Long fatorialEsperado = 120L;
		// Executar o m�todo que est� sendo testado e obter o resultado atual
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		// Comprar resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial0() throws NegocioException {
		Integer n = 0;
		Long fatorialEsperado = 1L;
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test(expected = NegocioException.class)
	public void testarFatorialMenos1() throws NegocioException {
		Integer n = -1;
		CalculoUtil.calcularFatorial(n);
	}

}
