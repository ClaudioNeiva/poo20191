package br.ucsal.bes2019.poo.avaliacao1.enums;

public enum SituacaoImovelEnum {
	VENDIDA, DISPONIVEL, BLOQUEADA;
}
