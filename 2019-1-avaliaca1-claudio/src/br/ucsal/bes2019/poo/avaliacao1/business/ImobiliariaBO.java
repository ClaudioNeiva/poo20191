package br.ucsal.bes2019.poo.avaliacao1.business;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes2019.poo.avaliacao1.domain.Apartamento;
import br.ucsal.bes2019.poo.avaliacao1.domain.Casa;
import br.ucsal.bes2019.poo.avaliacao1.domain.Imovel;
import br.ucsal.bes2019.poo.avaliacao1.enums.SituacaoImovelEnum;

public class ImobiliariaBO {

	private static List<Imovel> imoveis = new ArrayList<>();

	public static void cadastrarCasa(String endereco, String bairro, Double valor, SituacaoImovelEnum situacao,
			Double areaTerreno) {
		Casa casa = new Casa(endereco, bairro, valor, situacao, areaTerreno);
		imoveis.add(casa);
	}

	public static void cadastrarApartamento(String endereco, String bairro, Double valor, SituacaoImovelEnum situacao,
			Double fracaoIdeal, Double areaPrivativa) {
		Apartamento apartamento = new Apartamento(endereco, bairro, valor, situacao, fracaoIdeal, areaPrivativa);
		imoveis.add(apartamento);
	}

	public static void listar() {
		for (Imovel imovel : imoveis) {
			System.out.println("***************************");
			System.out.println("codigo=" + imovel.getCodigo());
			System.out.println("bairro=" + imovel.getBairro());
			System.out.println("valor=" + imovel.getValor());
			System.out.println("situacao=" + imovel.getSituacao());
		}
	}

}
