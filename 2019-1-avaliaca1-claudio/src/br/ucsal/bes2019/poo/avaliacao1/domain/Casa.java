package br.ucsal.bes2019.poo.avaliacao1.domain;

import br.ucsal.bes2019.poo.avaliacao1.enums.SituacaoImovelEnum;

public class Casa extends Imovel {

	private Double areaTerreno;

	public Casa(String endereco, String bairro, Double valor, SituacaoImovelEnum situacao, Double areaTerreno) {
		super(endereco, bairro, valor, situacao);
		this.areaTerreno = areaTerreno;
	}

	public Double getAreaTerreno() {
		return areaTerreno;
	}

	public void setAreaTerreno(Double areaTerreno) {
		this.areaTerreno = areaTerreno;
	}

}
