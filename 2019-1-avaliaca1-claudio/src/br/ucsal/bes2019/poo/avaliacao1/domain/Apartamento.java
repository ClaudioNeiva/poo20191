package br.ucsal.bes2019.poo.avaliacao1.domain;

import br.ucsal.bes2019.poo.avaliacao1.enums.SituacaoImovelEnum;

public class Apartamento extends Imovel {

	private Double fracaoIdeal;

	private Double areaPrivativa;

	public Apartamento(String endereco, String bairro, Double valor, SituacaoImovelEnum situacao, Double fracaoIdeal,
			Double areaPrivativa) {
		super(endereco, bairro, valor, situacao);
		this.fracaoIdeal = fracaoIdeal;
		this.areaPrivativa = areaPrivativa;
	}

	public Double getFracaoIdeal() {
		return fracaoIdeal;
	}

	public void setFracaoIdeal(Double fracaoIdeal) {
		this.fracaoIdeal = fracaoIdeal;
	}

	public Double getAreaPrivativa() {
		return areaPrivativa;
	}

	public void setAreaPrivativa(Double areaPrivativa) {
		this.areaPrivativa = areaPrivativa;
	}

}
