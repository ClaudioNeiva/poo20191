package br.ucsal.bes.poo20191.aula10;

public class Funcionario {

	private static Integer contador = 0;

	private Integer matricula;

	private String nome;

	private String email;

	private SexoEnum sexo;

	public Funcionario(String nome, String email, SexoEnum sexo) {
		this.nome = nome;
		this.email = email;
		this.sexo = sexo;
		gerarMatricula();
	}

	private void gerarMatricula() {
		contador++;
		this.matricula = contador;
	}

	public static Integer getContador() {
		return contador;
	}

	// M�todos est�ticos n�o podem manipular atributos de inst�ncia!!!!
	// O m�todo abaixo N�O compila!
	// public static Integer getMatricula() {
	// return matricula;
	// }

	public Integer getMatricula() {
		return matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public SexoEnum getSexo() {
		return sexo;
	}

	public void setSexo(SexoEnum sexo) {
		this.sexo = sexo;
	}

}
