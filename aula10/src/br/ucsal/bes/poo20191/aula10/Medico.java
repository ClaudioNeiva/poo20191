package br.ucsal.bes.poo20191.aula10;

public class Medico extends Funcionario {

	private String crm;

	private String especialidade;

	public Medico(String nome, String email, SexoEnum sexo, String crm, String especialidade) {
		super(nome, email, sexo);
		this.crm = crm;
		this.especialidade = especialidade;
	}

	public String getCrm() {
		return crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

}
