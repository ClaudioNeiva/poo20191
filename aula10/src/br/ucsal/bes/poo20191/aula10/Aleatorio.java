package br.ucsal.bes.poo20191.aula10;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Aleatorio {

	private static final int NUMERO_MAXIMO = 10;
	private static Random random = new Random();

	public static void main(String[] args) {
		gerarAleatoriosVetor();
		gerarAleatoriosList();
		gerarAleatoriosSet();
	}

	private static void gerarAleatoriosSet() {
		Set<Integer> numeros = new HashSet<>();
		Integer numero;
		do {
			numero = random.nextInt(NUMERO_MAXIMO + 1);
			numeros.add(numero);
		} while (numeros.size() < 5);
		System.out.println(numeros);
	}

	private static void gerarAleatoriosList() {
		List<Integer> numeros = new ArrayList<>();
		Integer numero;
		do {
			numero = random.nextInt(NUMERO_MAXIMO + 1);
			if (!numeros.contains(numero)) {
				numeros.add(numero);
			}
		} while (numeros.size() < 5);
		System.out.println(numeros);
	}

	private static void gerarAleatoriosVetor() {
		Integer[] numeros = new Integer[5];
		Integer qtd = 0;
		Integer numero;
		do {
			numero = random.nextInt(NUMERO_MAXIMO + 1);
			if (!existirNumeroVetor(numero, numeros, qtd)) {
				numeros[qtd] = numero;
				qtd++;
			}
		} while (qtd < 5);
		exibirVetor(numeros);
	}

	private static void exibirVetor(Integer[] vetor) {
		System.out.println("gerarAleatoriosVetor:");
		for (Integer numero : vetor) {
			System.out.println(numero);
		}
	}

	private static boolean existirNumeroVetor(Integer numero, Integer[] vetor, Integer qtd) {
		for (Integer i = 0; i < qtd; i++) {
			if (numero == vetor[i]) {
				return true;
			}
		}
		return false;
	}
}
