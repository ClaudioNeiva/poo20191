package br.ucsal.bes.poo20191.aula10;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FuncionarioExemplo {

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Contador de funcion�rios=" + Funcionario.getContador());

		// N�o faz sentido perguntar pela matr�cula, se nenhum funcion�rio foi
		// instanciado.
		// System.out.println("Funcionario.matricula=" +
		// Funcionario.getMatricula());

		Funcionario funcionario1 = new Funcionario("Claudio", "antonio@ucsal.br", SexoEnum.MASCULINO);
		Funcionario funcionario2 = new Funcionario("Maria", "maria@ucsal.br", SexoEnum.FEMININO);
		Funcionario funcionario3 = new Funcionario("Ana", "ana@ucsal.br", SexoEnum.FEMININO);
		Funcionario funcionario4 = cadastrarFuncionario();

		List<Funcionario> funcionarios = new ArrayList<>();
		funcionarios.add(new Funcionario("Pedro", "asda@asdas", SexoEnum.MASCULINO));
		funcionarios.add(cadastrarFuncionario());

		System.out.println("funcionario1.matricula=" + funcionario1.getMatricula());
		System.out.println("funcionario2.matricula=" + funcionario2.getMatricula());
		System.out.println("funcionario3.matricula=" + funcionario3.getMatricula());
		System.out.println("funcionario4.matricula=" + funcionario4.getMatricula());

		// N�o faz sentido perguntar � classe pela matr�cula, se existem 3
		// inst�ncias, cada uma com sua matr�cula.
		// System.out.println("Funcionario.matricula=" +
		// Funcionario.getMatricula());

		definirDireitoLicencaMaternidade(funcionario1);
		definirDireitoLicencaMaternidade(funcionario2);
		definirDireitoLicencaMaternidade(funcionario3);
		definirDireitoLicencaMaternidade(funcionario4);

	}

	private static Funcionario cadastrarFuncionario() {
		String nome;
		String email;
		String sexoString;
		SexoEnum sexo;
		System.out.println("Informe o nome:");
		nome = scanner.nextLine();
		System.out.println("Informe o email:");
		email = scanner.nextLine();
		System.out.println("Informe o sexo (" + SexoEnum.mostrarItens() + "):");
		sexoString = scanner.nextLine();
		sexo = SexoEnum.valueOf(sexoString.toUpperCase());
		Funcionario funcionario = new Funcionario(nome, email, sexo);
		return funcionario;
	}

	private static void definirDireitoLicencaMaternidade(Funcionario funcionario) {
		if (SexoEnum.FEMININO.equals(funcionario.getSexo())) {
			// if (funcionario.getSexo().equals(SexoEnum.FEMININO)) {
			System.out.println("Funcion�rio " + funcionario.getNome() + " tem direito � Licen�a Maternidade.");
		} else {
			System.out.println("Funcion�rio " + funcionario.getNome() + " N�O tem direito � Licen�a Maternidade.");
		}
	}

}
