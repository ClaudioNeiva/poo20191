package br.ucsal.bes.poo20191.aula10;

public enum SexoEnum {

	MASCULINO,

	FEMININO;

	public static String mostrarItens() {
		String itens = "";
		for (SexoEnum sexoEnum : values()) {
			itens += sexoEnum + ",";
		}
		return itens.substring(0, itens.length() - 1);

	}

}
